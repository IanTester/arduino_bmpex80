/*
BMP085.cpp - BMP085/BMP180/BMP185 class
Copyright (C) 2019 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "internal/common.h"
#include "internal/BMP085.h"

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include <Wire.h>

BMP085::BMP085() :
  I2Cdevice_base(BMP085_I2CADDR),
  TempSensor_base(),
  PressSensor_base()
{}

bool BMP085::begin(Mode m) {
  if (m > Mode::UltraHighRes)
    m = Mode::UltraHighRes;
  _oversampling = static_cast<uint8_t>(m);
  _oss_mult = 1 << _oversampling;
  _oss_scale = 50000UL >> _oversampling;

  if (!check())
    return false;

  /* read calibration data */
  _ac1 = static_cast<float>(_read<int16_t>(Register::Cal_AC1)) * 4;
  if (_i2c_error) return false;
  _ac2 = static_cast<float>(_read<int16_t>(Register::Cal_AC2)) / 32;
  if (_i2c_error) return false;
  _ac3 = static_cast<float>(_read<int16_t>(Register::Cal_AC3)) / 128;
  if (_i2c_error) return false;
  _ac4 = static_cast<float>(_read<uint16_t>(Register::Cal_AC4)) / 32768;
  if (_i2c_error) return false;
  _ac5 = static_cast<float>(_read<uint16_t>(Register::Cal_AC5)) / 32768;
  if (_i2c_error) return false;
  _ac6 = static_cast<float>(_read<uint16_t>(Register::Cal_AC6));
  if (_i2c_error) return false;

  _b1 = static_cast<float>(_read<int16_t>(Register::Cal_B1)) / 65536;
  if (_i2c_error) return false;
  _b2 = static_cast<float>(_read<int16_t>(Register::Cal_B2)) / 2048;
  if (_i2c_error) return false;

  _mb = static_cast<float>(_read<int16_t>(Register::Cal_MB));
  if (_i2c_error) return false;
  _mc = static_cast<float>(_read<int16_t>(Register::Cal_MC)) * 2048;
  if (_i2c_error) return false;
  _md = static_cast<float>(_read<int16_t>(Register::Cal_MD));
  if (_i2c_error) return false;

  return true;
}

bool BMP085::check(void) {
  uint8_t d = _read<uint8_t>(Register::Check);
  if (_i2c_error)
    return false;

  return d == 0x55;
}

unsigned long BMP085::measureTemperature(void) {
  _write(Register::Control, Command::ReadTemperature);
  _time_t = micros() + 4500;
  return 5;
}

bool BMP085::temperatureReadingReady(void) {
  return micros() > _time_t;
}

unsigned long BMP085::measurePressure(void) {
  _write(Register::Control, static_cast<Command>(static_cast<uint8_t>(Command::ReadPressure)
						 | (_oversampling << 6)));
  unsigned long ret;
  switch (_oversampling) {
  case 0:
    _time_p = micros() + 4500;
    ret = 5;
    break;

  case 1:
    _time_p = micros() + 7500;
    ret = 8;
    break;

  case 2:
    _time_p = micros() + 13500;
    ret = 14;
    break;

  default:
    _time_p = micros() +25500;
    ret = 26;
  }

  return ret;
}

bool BMP085::pressureReadingReady(void) {
  return micros() > _time_p;
}

Reading_base* BMP085::readTemperature(Reading_base* reading) {
  uint16_t d = _read<uint16_t>(Register::Data);
  if (_i2c_error)
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BMP085Reading(this));
  reading->_set_raw_temp(d);

  return reading;
}

Reading_base* BMP085::readPressure(Reading_base* reading) {
  uint8_t data[3];
  if (!_read(Register::Data, data, 3))
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BMP085Reading(this));
  uint32_t rp = ((uint32_t)data[0] << 16) | ((uint32_t)data[1] << 8) | (uint32_t)data[2];
  rp >>= (8 - _oversampling);
  reading->_set_raw_press(rp);

  return reading;
}


BMP085Reading::BMP085Reading(BMP085* s, uint32_t rt, uint32_t rp) :
  Reading_base(rt, rp),
  _sensor(s),
  _have_b5(false)
{}

void BMP085Reading::_computeB5(void) {
  float X1 = (_rt - _sensor->_ac6) * _sensor->_ac5;
  float X2 = _sensor->_mc / (X1 + _sensor->_md);
  _b5 = (X1 + X2) / 64;         // Now 1/64th the scale
  _have_b5 = true;
}

float BMP085Reading::temperature(void) {
  if (!_have_b5)
    _computeB5();

  return (_b5 + 0.125) * 0.4;
}

float BMP085Reading::pressure(void) {
  if (!_have_b5)
    _computeB5();

  // do pressure calcs
  float B6 = _b5 - 62.5;        // Now also 1/64th the scale
  float X1 = _sensor->_b2 * sqr(B6);
  float X2 = _sensor->_ac2 * B6;
  float X3 = X1 + X2;
  float B3 = (((_sensor->_ac1 + X3) * _sensor->_oss_mult) + 2) / 4;

  X1 = _sensor->_ac3 * B6;
  X2 = _sensor->_b1 * sqr(B6);
  X3 = ((X1 + X2) + 2) / 4;
  float B4 = _sensor->_ac4 * (X3 + 32768);
  float B7 = (_rp - B3) * _sensor->_oss_scale;

  float p = B7 * 2 / B4;
  X1 = sqr(p / 256) * 3038 / 65536;
  X2 = -7357 * p / 65536;

  p += (X1 + X2 + 3791) / 16;
  return p;
}
