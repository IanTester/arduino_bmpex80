/*
BMP388.cpp - BMP388 class
Copyright (C) 2019 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "internal/common.h"
#include "internal/BMP388.h"

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include <Wire.h>

BMP388::BMP388(uint8_t i2c_addr) :
  I2Cdevice_base(i2c_addr),
  TempSensor_base(),
  PressSensor_base()
{}

uint8_t BMP388::chip_id(void) {
  return _read<uint8_t>(Register::CHIP_ID);
}

bool BMP388::begin(void) {
  _par_t1 = (float)_read<uint16_t>(Register::PAR_T1) * 256.0;
  if (_i2c_error) return false;
  _par_t2 = (float)_read<uint16_t>(Register::PAR_T2) / 1073741824.0;
  if (_i2c_error) return false;
  _par_t3 = (float)_read<int8_t>(Register::PAR_T3) / 281474976710656.0;
  if (_i2c_error) return false;

  _par_p1 = ((float)_read<int16_t>(Register::PAR_P1) - 16384.0) / 1048576.0;
  if (_i2c_error) return false;
  _par_p2 = ((float)_read<int16_t>(Register::PAR_P2) - 16384.0) / 536870912.0;
  if (_i2c_error) return false;
  _par_p3 = (float)_read<int8_t>(Register::PAR_P3) / 4294967296.0;
  if (_i2c_error) return false;
  _par_p4 = (float)_read<int8_t>(Register::PAR_P4) / 137438953472.0;
  if (_i2c_error) return false;
  _par_p5 = (float)_read<uint16_t>(Register::PAR_P5) * 8.0;
  if (_i2c_error) return false;
  _par_p6 = (float)_read<uint16_t>(Register::PAR_P6) / 64.0;
  if (_i2c_error) return false;
  _par_p7 = (float)_read<int8_t>(Register::PAR_P7) / 256.0;
  if (_i2c_error) return false;
  _par_p8 = (float)_read<int8_t>(Register::PAR_P8) / 32768.0;
  if (_i2c_error) return false;
  _par_p9 = (float)_read<int16_t>(Register::PAR_P9) / 281474976710656.0;
  if (_i2c_error) return false;
  _par_p10 = (float)_read<int8_t>(Register::PAR_P10) / 281474976710656.0;
  if (_i2c_error) return false;
  _par_p11 = (float)_read<int8_t>(Register::PAR_P11) / 36893488147419103232.0;
  if (_i2c_error) return false;

  _read(Register::FIFO_CONFIG_1, _fifo_config, 2);
  _read(Register::INT_CTRL, &_int_ctrl, 1);
  _read(Register::IF_CONF, &_if_conf, 1);
  _read(Register::PWR_CTRL, &_pwr_ctrl, 1);
  _read(Register::OSR, &_osr, 1);
  _read(Register::ODR, &_odr, 1);
  _read(Register::CONFIG, &_config, 1);
}

#define GET(N, T, V, M) T BMP388::get_##N(void) { return static_cast<T>(V & static_cast<uint8_t>(M)); }
#define SET(N, T, V, M) void BMP388::set_##N(T val) {\
    V &= ~static_cast<uint8_t>(M);		    \
    V |= static_cast<uint8_t>(val);		    \
  }
#define SET_BOOL(N, V, M) void BMP388::set_##N(bool val) {		\
    if (val) {								\
      V |= static_cast<uint8_t>(M);					\
    } else {								\
      V &= ~static_cast<uint8_t>(M);					\
    }									\
  }

// ERR_REG
GET(fatal_error, bool, _read<uint8_t>(Register::ERR_REG), err_reg_mask::fatal_err);
GET(cmd_error, bool, _read<uint8_t>(Register::ERR_REG), err_reg_mask::cmd_err);
GET(conf_error, bool, _read<uint8_t>(Register::ERR_REG), err_reg_mask::conf_err);

// STATUS
GET(cmd_ready, bool, _read<uint8_t>(Register::STATUS), status_mask::cmd_rdy);
GET(data_ready_press, bool, _read<uint8_t>(Register::STATUS), status_mask::drdy_press);
GET(data_ready_temp, bool, _read<uint8_t>(Register::STATUS), status_mask::drdy_temp);

// Ask sensor to take measurements

unsigned long BMP388::_measurement_time(void) {
  uint8_t osrs_p = static_cast<uint8_t>(osr_p());
  uint8_t p = (osrs_p == 0 ? 0 : 1 << (osrs_p - 1));

  uint8_t osrs_t = static_cast<uint8_t>(osr_t()) >> 3;
  uint8_t t = (osrs_t == 0 ? 0 : 1 << (osrs_t - 1));

  return (2000 * t) + (2000 * p) + 1500;
}

unsigned long BMP388::measureAll(void) {
  _old_pwr_ctrl = _pwr_ctrl;
  set_mode(mode::forced);
  set_temp_en(true);
  set_press_en(true);
  write_pwr_ctrl();

  return _measurement_time();
}

bool BMP388::allReadingsReady(void) {
  uint8_t status = _read<uint8_t>(Register::STATUS);
  uint8_t mask = static_cast<uint8_t>(status_mask::drdy_press) | static_cast<uint8_t>(status_mask::drdy_temp);
  if ((status & mask) == mask) {
    _pwr_ctrl = _read<uint8_t>(Register::PWR_CTRL);
    set_temp_en(_old_pwr_ctrl & static_cast<uint8_t>(pwr_ctrl_mask::temp_en));
    set_press_en(_old_pwr_ctrl & static_cast<uint8_t>(pwr_ctrl_mask::press_en));
    return true;
  } else
    return false;
}

unsigned long BMP388::measureTemperature(void) {
  _old_pwr_ctrl = _pwr_ctrl;
  set_mode(mode::forced);
  set_temp_en(true);
  set_press_en(false);
  write_pwr_ctrl();

  return _measurement_time();
}

bool BMP388::temperatureReadingReady(void) {
  if (get_data_ready_temp()) {
    _pwr_ctrl = _read<uint8_t>(Register::PWR_CTRL);
    set_temp_en(_old_pwr_ctrl & static_cast<uint8_t>(pwr_ctrl_mask::temp_en));
    set_press_en(_old_pwr_ctrl & static_cast<uint8_t>(pwr_ctrl_mask::press_en));
    return true;
  } else
    return false;
}

unsigned long BMP388::measurePressure(void) {
  _old_pwr_ctrl = _pwr_ctrl;
  set_mode(mode::forced);
  set_temp_en(false);
  set_press_en(true);
  write_pwr_ctrl();

  return _measurement_time();
}

bool BMP388::pressureReadingReady(void) {
  if (get_data_ready_press()) {
    _pwr_ctrl = _read<uint8_t>(Register::PWR_CTRL);
    set_temp_en(_old_pwr_ctrl & static_cast<uint8_t>(pwr_ctrl_mask::temp_en));
    set_press_en(_old_pwr_ctrl & static_cast<uint8_t>(pwr_ctrl_mask::press_en));
    return true;
  } else
    return false;
}

Reading_base* BMP388::readAll(void) {
  uint8_t data[6];
  if (!_read(Register::PRESS_XLSB, data, 6))
    return nullptr;

  uint32_t rp = ((uint32_t)data[0] << 16) | ((uint32_t)data[1] << 8) | data[2];
  uint32_t rt = ((uint32_t)data[3] << 16) | ((uint32_t)data[4] << 8) | data[5];
  return (Reading_base*)(new BMP388Reading(this, rt, rp));
}

// PRESS_*
Reading_base* BMP388::readPressure(Reading_base* reading) {
  uint8_t data[3];
  if (!_read(Register::PRESS_XLSB, data, 3))
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BMP388Reading(this));
  reading->_set_raw_press(((uint32_t)data[0] << 16) | ((uint32_t)data[1] << 8) | data[2]);

  return reading;
}

// TEMP_*
Reading_base* BMP388::readTemperature(Reading_base* reading) {
  uint8_t data[3];
  if (!_read(Register::TEMP_XLSB, data, 3))
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BMP388Reading(this));
  reading->_set_raw_temp(((uint32_t)data[0] << 16) | ((uint32_t)data[1] << 8) | data[2]);

  return reading;
}

// SENSOR_TIME
uint32_t BMP388::sensor_time(void) {
  uint8_t data[4];
  if (!_read(Register::SENSOR_TIME_0, data, 4))
    return 0;

  return data[0]
    | ((uint32_t)data[1] << 8)
    | ((uint32_t)data[2] << 16)
    | ((uint32_t)data[3] << 24);
}

// EVENT
GET(por_detected, bool, _read<uint8_t>(Register::EVENT), event_mask::por_detected);

// INT_STATUS
GET(fwm_int, bool, _read<uint8_t>(Register::INT_STATUS), int_status_mask::fwm_int);
GET(ffull_int, bool, _read<uint8_t>(Register::INT_STATUS), int_status_mask::ffull_int);
GET(data_ready_int, bool, _read<uint8_t>(Register::INT_STATUS), int_status_mask::drdy);

// FIFO_LENGTH_*
uint16_t BMP388::get_fifo_byte_counter(void) {
  uint8_t data[2];
  _read(Register::FIFO_LENGTH_0, data, 2);
  return data[0] | ((uint16_t)(data[1] & 0x01) << 8);
}

// FIFO_DATA
uint8_t BMP388::get_fifo_data(void) {
  return _read<uint8_t>(Register::FIFO_DATA);
}

// FIFO_WTM_*
uint16_t BMP388::get_fifo_water_mark(void) {
  uint8_t data[2];
  _read(Register::FIFO_WTM_0, data, 2);
  return data[0] | ((uint16_t)(data[1] & 0x01) << 8);
}

void BMP388::set_fifo_water_mark(uint16_t val) {
  uint8_t data[2] = { (uint8_t)(val & 0xff), (uint8_t)((val >> 8) & 0x01) };
  _write(Register::FIFO_WTM_0, data, 2);
}

// FIFO_CONFIG_1
GET(fifo_mode, bool, _fifo_config[0], fifo_config_1_mask::fifo_mode);
SET_BOOL(fifo_mode, _fifo_config[0], fifo_config_1_mask::fifo_mode);

GET(fifo_stop_on_full, bool, _fifo_config[0], fifo_config_1_mask::fifo_stop_on_full);
SET_BOOL(fifo_stop_on_full, _fifo_config[0], fifo_config_1_mask::fifo_stop_on_full);

GET(fifo_time_en, bool, _fifo_config[0], fifo_config_1_mask::fifo_time_en);
SET_BOOL(fifo_time_en, _fifo_config[0], fifo_config_1_mask::fifo_time_en);

GET(fifo_press_en, bool, _fifo_config[0], fifo_config_1_mask::fifo_press_en);
SET_BOOL(fifo_press_en, _fifo_config[0], fifo_config_1_mask::fifo_press_en);

GET(fifo_temp_en, bool, _fifo_config[0], fifo_config_1_mask::fifo_temp_en);
SET_BOOL(fifo_temp_en, _fifo_config[0], fifo_config_1_mask::fifo_temp_en);

void BMP388::write_fifo_config_1(void) {
  _write(Register::FIFO_CONFIG_1, _fifo_config + 0, 1);
}

// FIFO_CONFIG_2
GET(data_select, BMP388::data_select, _fifo_config[1], fifo_config_2_mask::data_select);
SET(data_select, BMP388::data_select, _fifo_config[1], fifo_config_2_mask::data_select);

void BMP388::write_fifo_config_2(void) {
  _write(Register::FIFO_CONFIG_1, _fifo_config + 1, 1);
}

// INT_CTRL
GET(int_od, bool, _int_ctrl, int_ctrl_mask::int_od);
SET_BOOL(int_od, _int_ctrl, int_ctrl_mask::int_od);

GET(int_level, bool, _int_ctrl, int_ctrl_mask::int_level);
SET_BOOL(int_level, _int_ctrl, int_ctrl_mask::int_level);

GET(int_latch, bool, _int_ctrl, int_ctrl_mask::int_latch);
SET_BOOL(int_latch, _int_ctrl, int_ctrl_mask::int_latch);

GET(fifo_watermark_en, bool, _int_ctrl, int_ctrl_mask::fwtm_en);
SET_BOOL(fifo_watermark_en, _int_ctrl, int_ctrl_mask::fwtm_en);

GET(fifo_full_en, bool, _int_ctrl, int_ctrl_mask::ffull_en);
SET_BOOL(fifo_full_en, _int_ctrl, int_ctrl_mask::ffull_en);

GET(data_ready_en, bool, _int_ctrl, int_ctrl_mask::drdy_en);
SET_BOOL(data_ready_en, _int_ctrl, int_ctrl_mask::drdy_en);

void BMP388::write_int_ctrl(void) {
  _write(Register::INT_CTRL, &_int_ctrl, 1);
}

// IF_CONF
GET(spi3, bool, _if_conf, if_conf_mask::spi3);
SET_BOOL(spi3, _if_conf, if_conf_mask::spi3);

GET(i2c_wdt_en, bool, _if_conf, if_conf_mask::i2c_wdt_en);
SET_BOOL(i2c_wdt_en, _if_conf, if_conf_mask::i2c_wdt_en);

GET(i2c_wdt_sel, bool, _if_conf, if_conf_mask::i2c_wdt_sel);
SET_BOOL(i2c_wdt_sel, _if_conf, if_conf_mask::i2c_wdt_sel);

void BMP388::write_if_conf(void) {
  _write(Register::IF_CONF, &_if_conf, 1);
}

// PWR_CTRL
GET(press_en, bool, _pwr_ctrl, pwr_ctrl_mask::press_en);
SET_BOOL(press_en, _pwr_ctrl, pwr_ctrl_mask::press_en);

GET(temp_en, bool, _pwr_ctrl, pwr_ctrl_mask::temp_en);
SET_BOOL(temp_en, _pwr_ctrl, pwr_ctrl_mask::temp_en);

GET(mode, BMP388::mode, _pwr_ctrl, pwr_ctrl_mask::mode);
SET(mode, BMP388::mode, _pwr_ctrl, pwr_ctrl_mask::mode);

void BMP388::write_pwr_ctrl(void) {
  _write(Register::PWR_CTRL, &_pwr_ctrl, 1);
}

// OSR
GET(osr_p, BMP388::osr_p, _osr, osr_mask::osr_p);
SET(osr_p, BMP388::osr_p, _osr, osr_mask::osr_p);

GET(osr_t, BMP388::osr_t, _osr, osr_mask::osr_t);
SET(osr_t, BMP388::osr_t, _osr, osr_mask::osr_t);

void BMP388::write_osr(void) {
  _write(Register::OSR, &_osr, 1);
}

// ODR
GET(odr_sel, BMP388::odr_sel, _read<uint8_t>(Register::ODR), odr_mask::odr_sel);

void BMP388::set_odr_sel(odr_sel val) {
  uint8_t data = static_cast<uint8_t>(val);
  _write(Register::ODR, data);
}

// CONFIG
GET(iir_filter, BMP388::iir_filter, _read<uint8_t>(Register::CONFIG), config_mask::iir_filter);

void BMP388::set_iir_filter(iir_filter val) {
  uint8_t data = static_cast<uint8_t>(val);
  _write(Register::CONFIG, data);
}


BMP388Reading::BMP388Reading(BMP388* s, uint32_t rt, uint32_t rp) :
  Reading_base(rt, rp),
  _sensor(s),
  _have_temp(false)
{}

// Conversion methods

float BMP388Reading::temperature(void) {
  float var1 = (float)(_rt - _sensor->_par_t1);
  float var2 = (float)(var1 * _sensor->_par_t2);
  _temp = var2 + sqr(var1) * _sensor->_par_t3;
  _have_temp = true;

  return _temp;
}

float BMP388Reading::pressure(void) {
  if (!_have_temp)
    temperature();

  float var1 = _sensor->_par_p6 * _temp;
  float var2 = _sensor->_par_p7 * sqr(_temp);
  float var3 = _sensor->_par_p8 * cube(_temp);
  float out1 = _sensor->_par_p5 + var1 + var2 + var3;

  var1 = _sensor->_par_p2 * _temp;
  var2 = _sensor->_par_p3 * sqr(_temp);
  var3 = _sensor->_par_p4 * cube(_temp);

  float out2 = (float)_rp * (_sensor->_par_p1 + var1 + var2 + var3);
  var1 = sqr((float)_rp);
  var2 = _sensor->_par_p9 + _sensor->_par_p10 * _temp;
  var3 = var1 * var2;

  return out1 + out2 + var3 + cube((float)_rp) * _sensor->_par_p11;
}
