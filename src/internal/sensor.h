/*
BMPEx80/src/internal/sensor.h - base classes for sensors
Copyright (C) 2019-2020 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include <stdint.h>
#include "internal/reading.h"

//! Abstract base class for any sensor
class Sensor_base {
protected:

public:
  //! Empty constructor
  Sensor_base() {}

  //! Does this class implement the measureAll() method?
  inline virtual bool can_MeasureAll(void) { return false; }

  //! Ask sensor to measure all aspects
  inline virtual unsigned long measureAll(void) { return 0; }

  //! Are the readings triggered by measureAll() ready to read?
  virtual bool allReadingsReady(void) { return false; }

  /*! Read all readings
    @return Reading pointer
   */
  inline virtual Reading_base* readAll(void) { return nullptr; }

}; // class Sensor_base


//! Abstract base class for temperature sensors
class TempSensor_base {
protected:

public:
  //! Empty constructor
  TempSensor_base() {}

  /*! Ask sensor to measure the temperature
    @return Number of milliseconds until data should be available
  */
  virtual unsigned long measureTemperature(void) { return 0; }

  //! Is the temperature reading triggered by measureTemperature() ready to read?
  virtual bool temperatureReadingReady(void) { return false; }

  /*! Read the temperature value
    @return Reading pointer
   */
  virtual Reading_base* readTemperature(Reading_base* reading = nullptr) { return nullptr; }

}; // class TempSensor_base


//! Abstract base class for pressure sensors
class PressSensor_base {
protected:

public:
  //! Empty constructor
  PressSensor_base() {}

  /*! Ask sensor to measure the air pressure
    @return Number of milliseconds until data should be available
  */
  virtual unsigned long measurePressure(void) { return 0; }

  //! Is the pressure reading triggered by measurePressure() ready to read?
  virtual bool pressureReadingReady(void) { return false; }

  /*! Read the air pressure value
    @return Reading pointer
   */
  virtual Reading_base* readPressure(Reading_base* reading = nullptr) { return nullptr; }

}; // class PressSensor_base


//! Abstract base class for humidity sensors
class HumSensor_base {
protected:

public:
  //! Empty constructor
  HumSensor_base() {}

  /*! Ask sensor to measure the humidity
    @return Number of milliseconds until data should be available
  */
  virtual unsigned long measureHumidity(void) { return 0; }

  //! Is the humdidity reading triggered by measureHumidity() ready to read?
  virtual bool humidityReadingReady(void) { return false; }

  /*! Read the humidity value
    @return Reading pointer
   */
  virtual Reading_base* readHumidity(Reading_base* reading = nullptr) { return nullptr; }

}; // class HumSensor_base


//! Abstract base class for "gas" sensors
class GasSensor_base {
protected:

public:
  //! Empty constructor
  GasSensor_base() {}

  /*! Ask sensor to measure the "gas"
    @return Number of milliseconds until data should be available
  */
  virtual unsigned long measureGas(void) { return 0; }

  //! Is the "gas" reading triggered by measureGas() ready to read?
  virtual bool gasReadingReady(void) { return false; }

  /*! Read the gas value
    @return Reading pointer
   */
  virtual Reading_base* readGas(Reading_base* reading = nullptr) { return nullptr; }

}; // class GasSensor_base
