/*
BMPEx80/src/internal/i2cdevice.h - base class for I2C devices
Copyright (C) 2019-2020 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include <stdint.h>

//! Abstract base class for I2C devices
class I2Cdevice_base {
protected:
  uint8_t _i2c_addr;
  bool _i2c_error;

  //! Read an arbitrary number of bytes from a range of registers
  bool _read(uint8_t reg, uint8_t* data, uint8_t len);

  //! A template form that returns the desired type
  template<typename T>
  T _read(uint8_t reg);

  //! Write a single byte to a register
  void _write(uint8_t reg, uint8_t val);

  //! Write an arbitrary number of bytes to a range of registers
  void _write(uint8_t reg, uint8_t* data, uint8_t len);

  template<typename R>
  inline bool _read(R reg, uint8_t* data, uint8_t len) { return _read(static_cast<uint8_t>(reg), data, len); }

  template<typename T, typename R>
  inline T _read(R reg) { return _read<T>(static_cast<uint8_t>(reg)); }

  template<typename R, typename V>
  inline void _write(R reg, V val) { _write(static_cast<uint8_t>(reg), static_cast<uint8_t>(val)); }

  template<typename R>
  inline void _write(R reg, uint8_t* data, uint8_t len) { _write(static_cast<uint8_t>(reg), data, len); }

public:
  I2Cdevice_base(uint8_t addr);

}; // class I2Cdevice_base
