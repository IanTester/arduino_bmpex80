/*
BMPEx80/src/internal/reading.h - base class for sensor reading
Copyright (C) 2019-2020 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include <stdint.h>

class BMP085;
class BMP280;
class BMP388;
class BME280;
class BME680;

//! Abstract base class for any reading
class Reading_base {
protected:
  uint32_t _rt, _rp, _rh, _rg;
  bool _have_rt, _have_rp, _have_rh, _have_rg;

  void _set_raw_temp(uint32_t rt) {
    _rt = rt;
    _have_rt = ((rt & 0x80000000) == 0) || (rt & 0x000fffff);
  }

  void _set_raw_press(uint32_t rp) {
    _rp = rp;
    _have_rp = ((rp & 0x80000000) == 0) || (rp & 0x000fffff);
  }

  void _set_raw_hum(uint32_t rh) {
    _rh = rh;
    _have_rh = ((rh & 0x80000000) == 0) || (rh & 0x000fffff);
  }

  void _set_raw_gas(uint32_t rg) {
    _rg = rg;
    _have_rg = ((rg & 0x80000000) == 0) || (rg & 0x000fffff);
  }

  friend class BMP085;
  friend class BMP280;
  friend class BMP388;
  friend class BME280;
  friend class BME680;


public:
  //! Constructor
  Reading_base(uint32_t rt = 0x80000000, uint32_t rp = 0x80000000, uint32_t rh = 0x80000000, uint32_t rg = 0x80000000) :
    _rt(rt),
    _rp(rp),
    _rh(rh),
    _rg(rg),
    _have_rt(((rt & 0x80000000) == 0) || (rt & 0x000fffff)),
    _have_rp(((rp & 0x80000000) == 0) || (rp & 0x000fffff)),
    _have_rh(((rh & 0x80000000) == 0) || (rh & 0x000fffff)),
    _have_rg(((rg & 0x80000000) == 0) || (rg & 0x000fffff))
  {}

  //! Return the raw temperature value
  uint32_t rawTemperature(void) const { return _rt; }

  //! Return the raw air pressure value
  uint32_t rawPressure(void) const { return _rp; }

  //! Return the raw humidity value
  uint32_t rawHumidity(void) const { return _rh; }

  //! Return the raw gas value
  uint32_t rawGas(void) const { return _rg; }

  //! Does this reading have a temperature value?
  bool haveTemperature(void) const { return _have_rt; }

  //! Does this reading have a pressure value?
  bool havePressure(void) const { return _have_rp; }

  //! Does this reading have a humidity value?
  bool haveHumitidy(void) const { return _have_rh; }

  //! Does this reading have a gas value?
  bool haveGas(void) const { return _have_rg; }


  //! Convert the raw temperature reading into degrees Celcius
  virtual float temperature(void);

  //! Convert the raw air pressure reading into pascals
  virtual float pressure(void);

  /*! Correct air pressure for altitude
    @param altitude Altitude of reading, in metres above sea level
    @return Pressure at sea level
   */
  float sealevelPressure(float altitude = 0);

  /*! Estimate altitude base on air pressure
    @param sealevelPressure Air pressure at sea level, in pascals
    @return Altitude, in metres above sea level
   */
  float altitude(float sealevelPressure = 101325);

  //! Convert the raw humidity reading into relative humidity percentage
  virtual float humidity(void);

  //! Convert the raw gas reading into ?
  virtual float gas(void);

}; // class Reading_base


