/*
BMPEx80/src/internal/i2cdevice.cpp - base class for I2C devices
Copyright (C) 2019-2020 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "internal/i2cdevice.h"

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include <Wire.h>

I2Cdevice_base::I2Cdevice_base(uint8_t addr) :
  _i2c_addr(addr),
  _i2c_error(false)
{}

bool I2Cdevice_base::_read(uint8_t addr, uint8_t* data, uint8_t len) {
  Wire.beginTransmission(_i2c_addr);
  Wire.write(addr);
  Wire.endTransmission();

  _i2c_error = false;
  Wire.beginTransmission(_i2c_addr);
  if (Wire.requestFrom(_i2c_addr, len) < len) {
    _i2c_error = true;
    return false;
  }

  for (uint8_t i = 0; i < len; i++) {
    int b = Wire.read();
    if (b == -1) {
      _i2c_error = true;
      return false;
    }
    data[i] = b & 0xff;
  }
  Wire.endTransmission();

  return true;
}

template<>
uint8_t I2Cdevice_base::_read<uint8_t>(uint8_t addr) {
  uint8_t data;
  if (!_read(addr, &data, 1))
    return 0;

  return data;
}

template<>
int8_t I2Cdevice_base::_read<int8_t>(uint8_t addr) {
  uint8_t uval = _read<uint8_t>(addr);
  return *((int8_t*)&uval);
}

template<>
uint16_t I2Cdevice_base::_read<uint16_t>(uint8_t addr) {
  uint8_t data[2];
  if (!_read(addr, data, 2))
    return 0;

  return ((uint16_t)data[0] << 8) | data[1];
}

template<>
int16_t I2Cdevice_base::_read<int16_t>(uint8_t addr) {
  uint16_t uval = _read<uint16_t>(addr);
  return *((int16_t*)&uval);
}

void I2Cdevice_base::_write(uint8_t addr, uint8_t val) {
  Wire.beginTransmission(_i2c_addr);
  Wire.write(addr);
  Wire.write(val);
  Wire.endTransmission();
}

void I2Cdevice_base::_write(uint8_t addr, uint8_t* data, uint8_t len) {
  Wire.beginTransmission(_i2c_addr);
  Wire.write(addr);
  Wire.write(data, len);
  Wire.endTransmission();
}

