/*
BMP388.h - BMP388 class
Copyright (C) 2019 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include <stdint.h>
#include "internal/i2cdevice.h"
#include "internal/sensor.h"
#include "internal/reading.h"

#define GET(N, T) T get_##N(void)
#define SET(N, T) void set_##N(T val)
#define ACCESSOR(N, T) T get_##N(void);		\
  void set_##N(T val)

class BMP388Reading;

class BMP388 : public I2Cdevice_base, public TempSensor_base, public PressSensor_base {
private:
  uint8_t _fifo_config[2], _int_ctrl, _if_conf, _pwr_ctrl, _osr, _odr, _config, _old_pwr_ctrl;

  friend BMP388Reading;
  float _par_t1, _par_t2, _par_t3;
  float _par_p1, _par_p2, _par_p3, _par_p4, _par_p5, _par_p6, _par_p7, _par_p8, _par_p9, _par_p10, _par_p11;

  enum class Register : uint8_t {
    CHIP_ID		= 0x00,	// R/O

    ERR_REG		= 0x02,	// R/O
    STATUS		= 0x03,	// R/O
    PRESS_XLSB		= 0x04,	// R/O,
    PRESS_LSB		= 0x05,	// R/O,
    PRESS_MSB		= 0x06,	// R/O,
    TEMP_XLSB		= 0x07,	// R/O,
    TEMP_LSB		= 0x08,	// R/O,
    TEMP_MSB		= 0x09,	// R/O,

    SENSOR_TIME_0	= 0x0c,	// R/O,
    SENSOR_TIME_1	= 0x0d,	// R/O,
    SENSOR_TIME_2	= 0x0e,	// R/O,
    SENSOR_TIME_3	= 0x0f,	// R/O,
    EVENT		= 0x10, // R/O
    INT_STATUS		= 0x11,	// R/O
    FIFO_LENGTH_0	= 0x12,	// R/O
    FIFO_LENGTH_1	= 0x13,	// R/O
    FIFO_DATA		= 0x14,	// R/O
    FIFO_WTM_0		= 0x15,	// R/W
    FIFO_WTM_1		= 0x16,	// R/W
    FIFO_CONFIG_1	= 0x17,	// R/W
    FIFO_CONFIG_2	= 0x18,	// R/W
    INT_CTRL		= 0x19,	// R/W
    IF_CONF		= 0x1a,	// R/W
    PWR_CTRL		= 0x1b,	// R/W
    OSR			= 0x1c,	// R/W
    ODR			= 0x1d,	// R/W

    CONFIG		= 0x1f,	// R/W

    PAR_T1		= 0x31,	// R/O, 2 bytes unsigned

    PAR_T2		= 0x33,	// R/O, 2 bytes unsigned

    PAR_T3		= 0x35,	// R/O, 1 byte signed
    PAR_P1		= 0x36,	// R/O, 2 bytes signed

    PAR_P2		= 0x38,	// R/O, 2 bytes signed

    PAR_P3		= 0x3a,	// R/O, 1 byte signed
    PAR_P4		= 0x3b,	// R/O, 1 byte signed
    PAR_P5		= 0x3c,	// R/O, 2 bytes unsigned

    PAR_P6		= 0x3e,	// R/O, 2 bytes unsigned

    PAR_P7		= 0x40,	// R/O, 1 byte signed
    PAR_P8		= 0x41,	// R/O, 1 byte signed
    PAR_P9		= 0x42,	// R/O, 2 bytes signed

    PAR_P10		= 0x44,	// R/O, 1 bytes signed
    PAR_P11		= 0x45,	// R/O, 1 bytes signed

    CMD			= 0x7e,	// R/W
  };

  enum class chip_id_mask : uint8_t {
    chip_id_nvm		= 0x0f,
    chip_id_fixed	= 0xf0,
  };

  enum class err_reg_mask : uint8_t {
    fatal_err		= 0x01,
    cmd_err		= 0x02,
    conf_err		= 0x04,
  };

  enum class status_mask : uint8_t {
    cmd_rdy		= 0x10,
    drdy_press		= 0x20,
    drdy_temp		= 0x40,
  };

  enum class event_mask : uint8_t {
    por_detected	= 0x01,
  };

  enum class int_status_mask : uint8_t {
    fwm_int		= 0x01,
    ffull_int		= 0x02,

    drdy		= 0x08,
  };

  enum class fifo_config_1_mask : uint8_t {
    fifo_mode		= 0x01,
    fifo_stop_on_full	= 0x02,
    fifo_time_en	= 0x04,
    fifo_press_en	= 0x08,
    fifo_temp_en	= 0x10,
  };

  enum class fifo_config_2_mask : uint8_t {
    fifo_subsampling	= 0x07,
    data_select		= 0x18,
  };

  enum class int_ctrl_mask : uint8_t {
    int_od		= 0x01,
    int_level		= 0x02,
    int_latch		= 0x04,
    fwtm_en		= 0x08,
    ffull_en		= 0x10,

    drdy_en		= 0x40,
  };

  enum class if_conf_mask : uint8_t {
    spi3		= 0x01,
    i2c_wdt_en		= 0x02,
    i2c_wdt_sel		= 0x04,
  };

  enum class pwr_ctrl_mask : uint8_t {
    press_en		= 0x01,
    temp_en		= 0x02,

    mode		= 0x30,
  };

  enum class osr_mask : uint8_t {
    osr_p		= 0x07,
    osr_t		= 0x38,
  };

  enum class odr_mask : uint8_t {
    odr_sel		= 0x1f,
  };

  enum class config_mask : uint8_t {
    iir_filter		= 0x0e,
  };

  enum class cmd : uint8_t {
    nop			= 0x00,

    extmode_en_middle	= 0x34,

    fifo_flush		= 0xb0,

    softreset		= 0xb6,
  };

  unsigned long _measurement_time(void);


public:
  //! Constructor
  BMP388(uint8_t i2c_addr);

  uint8_t chip_id(void);

  //! Read calibration data and config registers
  bool begin(void);

  // ERR_REG
  GET(fatal_error, bool);
  GET(cmd_error, bool);
  GET(conf_error, bool);

  // STATUS
  GET(cmd_ready, bool);
  GET(data_ready_press, bool);
  GET(data_ready_temp, bool);

  inline virtual bool can_MeasurAll(void) { return true; }

  virtual unsigned long measureAll(void);
  virtual bool allReadingsReady(void);

  virtual unsigned long measureTemperature(void);
  virtual bool temperatureReadingReady(void);

  virtual unsigned long measurePressure(void);
  virtual bool pressureReadingReady(void);

  virtual Reading_base* readAll(void);

  // PRESS_*
  virtual Reading_base* readPressure(Reading_base* reading = nullptr);

  // TEMP_*
  virtual Reading_base* readTemperature(Reading_base* reading = nullptr);

  // SENSOR_TIME
  uint32_t sensor_time(void);

  // EVENT
  GET(por_detected, bool);

  // INT_STATUS
  GET(fwm_int, bool);
  GET(ffull_int, bool);
  GET(data_ready_int, bool);

  // FIFO_LENGTH_*
  GET(fifo_byte_counter, uint16_t);

  // FIFO_DATA
  GET(fifo_data, uint8_t);

  // FIFO_WTM_*
  ACCESSOR(fifo_water_mark, uint16_t);

  // FIFO_CONFIG_1
  ACCESSOR(fifo_mode, bool);
  ACCESSOR(fifo_stop_on_full, bool);
  ACCESSOR(fifo_time_en, bool);
  ACCESSOR(fifo_press_en, bool);
  ACCESSOR(fifo_temp_en, bool);
  void write_fifo_config_1(void);

  // FIFO_CONFIG_2
  enum class fifo_subsampling : uint8_t {
    // TODO
  };

  enum class data_select : uint8_t {
    unfiltered		= 0x00,
    filtered		= 0x08,
    _reserved1		= 0x10,
    _reserved2		= 0x18,
  };

  ACCESSOR(data_select, data_select);
  void write_fifo_config_2(void);

  // INT_CTRL
  ACCESSOR(int_od, bool);
  ACCESSOR(int_level, bool);
  ACCESSOR(int_latch, bool);

  ACCESSOR(fifo_watermark_en, bool);
  ACCESSOR(fifo_full_en, bool);
  ACCESSOR(data_ready_en, bool);
  void write_int_ctrl(void);

  // IF_CONF
  ACCESSOR(spi3, bool);
  ACCESSOR(i2c_wdt_en, bool);
  ACCESSOR(i2c_wdt_sel, bool);
  void write_if_conf(void);

  // PWR_CTRL
  enum class mode : uint8_t {
    sleep		= 0x00,
    forced		= 0x10,
    forced2		= 0x20,	// same as 'forced'
    normal		= 0x30,
  };

  ACCESSOR(press_en, bool);
  ACCESSOR(temp_en, bool);
  ACCESSOR(mode, mode);
  void write_pwr_ctrl(void);

  // OSR
  enum class osr_p : uint8_t {
    x1			= 0x00,
    x2			= 0x01,
    x4			= 0x02,
    x8			= 0x03,
    x16			= 0x04,
    x32			= 0x05,
    _reserved1		= 0x06,
    _reserved2		= 0x07,
  };

  enum class osr_t : uint8_t {
    x1			= 0x00,
    x2			= 0x08,
    x4			= 0x10,
    x8			= 0x18,
    x16			= 0x20,
    x32			= 0x28,
    _reserved1		= 0x30,
    _reserved2		= 0x38,
  };

  ACCESSOR(osr_p, osr_p);
  ACCESSOR(osr_t, osr_t);
  void write_osr(void);

  // ODR
  enum class odr_sel : uint8_t {
    ODR_200		= 0x00,
    ODR_100		= 0x01,
    ODR_50		= 0x02,
    ODR_25		= 0x03,
    ODR_12p5		= 0x04,
    ODR_6p25		= 0x05,
    ODR_3p1		= 0x06,
    ODR_1p5		= 0x07,
    ODR_0p78		= 0x08,
    ODR_0p39		= 0x09,
    ODR_0p2		= 0x0a,
    ODR_0p1		= 0x0b,
    ODR_0p05		= 0x0c,
    ODR_0p02		= 0x0d,
    ODR_0p01		= 0x0e,
    ODR_0p006		= 0x0f,
    ODR_0p003		= 0x10,
    ODR_0p0015		= 0x11,
  };

  ACCESSOR(odr_sel, odr_sel);

  // CONFIG
  enum class iir_filter : uint8_t {
    coef_0		= 0x00,
    coef_1		= 0x02,
    coef_3		= 0x04,
    coef_7		= 0x06,
    coef_15		= 0x08,
    coef_31		= 0x0a,
    coef_63		= 0x0c,
    coef_127		= 0x0e,
  };

  ACCESSOR(iir_filter, iir_filter);

}; // class BMP388


class BMP388Reading : public Reading_base {
protected:
  BMP388 *_sensor;
  float _temp;
  bool _have_temp;

public:
  //! Constructor for temperature and pressure
  BMP388Reading(BMP388* s, uint32_t rt = 0x80000000, uint32_t rp = 0x80000000);

  virtual float temperature(void);
  virtual float pressure(void);

}; // class BMP388Reading

#undef GET
#undef SET
#undef ACCESSOR
