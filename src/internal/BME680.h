/*
BME680.h - BME680 class
Copyright (C) 2019 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include <stdint.h>
#include "internal/i2cdevice.h"
#include "internal/sensor.h"
#include "internal/reading.h"

#define GET(N, T) T get_##N(void)
#define SET(N, T) void set_##N(T val)
#define ACCESSOR(N, T) T get_##N(void);		\
  void set_##N(T val)

class BME680Reading;

class BME680 : public I2Cdevice_base, public TempSensor_base, public PressSensor_base, public HumSensor_base, public GasSensor_base {
private:
  uint8_t _ctrl_gas_0, _ctrl_gas_1, _ctrl_hum, _ctrl_meas, _config, _status;
  uint8_t _old_ctrl_meas, _old_ctrl_hum;

  friend BME680Reading;
  uint8_t _res_heat_range;
  int8_t _res_heat_val, _range_sw_err;

#ifdef COMPUTE_INT
  unsigned short _par_t1, _par_p1, _par_h1, _par_h2;
  short _par_t2, _par_p2, _par_p4, _par_p5, _par_p8, _par_p9, _par_gh2;
  unsigned char _par_p10, _par_h6;
  char _par_t3, _par_p3, _par_p6, _par_p7, _par_h3, _par_h4, _par_h5, _par_h7, _par_gh1, _par_gh3;
#else
  float _par_t1, _par_t2, _par_t3;
  float _par_p1, _par_p2, _par_p3, _par_p4, _par_p5, _par_p6, _par_p7, _par_p8, _par_p9, _par_p10;
  float _par_h1, _par_h2, _par_h3, _par_h4, _par_h5, _par_h6, _par_h7;
  float _par_gh1, _par_gh2, _par_gh3;
#endif

  enum class Register : uint8_t {
    res_heat_val		= 0x00,	// R/O, Taken from Bosch's driver
    res_heat_range		= 0x02,	// R/O, Taken from Bosch's driver
    range_sw_err		= 0x04,	// R/O, Taken from Bosch's driver

    eas_status_0		= 0x1d,	// R/O
    press_msb			= 0x1f, // R/O
    press_lsb			= 0x20, // R/O
    press_xlsb			= 0x21, // R/O
    temp_msb			= 0x22, // R/O
    temp_lsb			= 0x23, // R/O
    temp_xlsb			= 0x24, // R/O
    hum_msb			= 0x25, // R/O
    hum_lsb			= 0x26, // R/O

    gas_msb			= 0x2a, // R/O
    gas_lsb			= 0x2b, // R/O

    idac_heat_0			= 0x50,	// R/W
    // 10 in total

    res_heat_0			= 0x5a,	// R/W
    // 10 in total

    gas_wait_0			= 0x64,	// R/W
    // 10 in total

    ctrl_gas_0			= 0x70,	// R/W
    ctrl_gas_1			= 0x71,	// R/W
    ctrl_hum			= 0x72,	// R/W
    status			= 0x73,	// R/O
    ctrl_meas			= 0x74,	// R/W
    config			= 0x75,	// R/W

    // First set of calibration parameters, taken from Bosch's driver
    par_T2			= 0x8a,	// R/O, signed short
    par_T3			= 0x8c,	// R/O, signed char
    par_P1			= 0x8e,	// R/O, unsigned short
    par_P2			= 0x90,	// R/O, signed short
    par_P3			= 0x92,	// R/O, signed char
    par_P4			= 0x94,	// R/O, signed short
    par_P5			= 0x96,	// R/O, signed short
    par_P7			= 0x98,	// R/O, signed char
    par_P6			= 0x99,	// R/O, signed char
    par_P8			= 0x9c,	// R/O, signed short
    par_P9			= 0x9e,	// R/O, signed short
    par_P10			= 0xa0,	// R/O, unsigned char

    id				= 0xd0, // R/O

    reset			= 0xe0,	// R/W

    // Second set of calibration parameters, taken from Bosch's driver
    par_H2			= 0xe1,	// R/O, unsigned short (big-endian!)
    par_H1			= 0xe2,	// R/O, unsigned short
    par_H3			= 0xe4,	// R/O, signed char
    par_H4			= 0xe5,	// R/O, signed char
    par_H5			= 0xe6,	// R/O, signed char
    par_H6			= 0xe7,	// R/O, unsigned char
    par_H7			= 0xe8,	// R/O, signed char
    par_T1			= 0xe9,	// R/O, unsigned short
    par_GH2			= 0xeb,	// R/O, signed short
    par_GH1			= 0xed,	// R/O, signed char
    par_GH3			= 0xee,	// R/O, signed char

  };

  // Taken from Bosch's driver
  enum class res_heat_range_mask : uint8_t {
    res_heat_range		= 0x30,
  };

  // Taken from Bosch's driver
  enum class range_sw_err_mask : uint8_t {
    range_sw_err		= 0xf0,
  };

  enum class eas_status_0_mask : uint8_t {
    gas_meas_index_0		= 0x0f,

    measuring			= 0x20,
    gas_measuring		= 0x40,
    new_data_0			= 0x80,
  };

  enum class gas_lsb_mask : uint8_t {
    gas_range			= 0x0f,
    heat_stab			= 0x10,
    gas_valid			= 0x20,
  };

  enum class ctrl_gas_0_mask : uint8_t {
    heat_off			= 0x08,
  };

  enum class ctrl_gas_1_mask : uint8_t {
    nb_conv			= 0x0f,
    run_gas			= 0x10,
  };

  enum class ctrl_hum_mask : uint8_t {
    osrs_h			= 0x07,

    spi_w3_int_en		= 0x40,
  };

  enum class status_mask : uint8_t {
    spi_mem_page		= 0x10,
  };

  enum class ctrl_meas_mask : uint8_t {
    mode			= 0x03,
    osrs_p			= 0x1c,
    osrs_t			= 0xe0,
  };

  enum class config_mask : uint8_t {
    spi_3w_en			= 0x01,

    filter			= 0x1c,
  };


public:
  BME680(uint8_t i2c_addr);

  //! Read calibration data and config registers
  bool begin(void);

  // eas_status_0
  GET(gas_measurement_index, uint8_t);
  GET(measuring, bool);
  GET(gas_measuring, bool);
  GET(new_data_0, bool);

  inline virtual bool can_MeasureAll(void) { return true; }

  virtual unsigned long measureAll(void);
  virtual bool allReadingsReady(void);

  virtual unsigned long measureTemperature(void);
  virtual bool temperatureReadingReady(void);

  virtual unsigned long measurePressure(void);
  virtual bool pressureReadingReady(void);

  virtual unsigned long measureHumidity(void);
  virtual bool humidityReadingReady(void);

  virtual unsigned long measureGas(void);
  virtual bool gasReadingReady(void);

  virtual Reading_base* readAll(void);

  // press_*
  virtual Reading_base* readPressure(Reading_base* reading = nullptr);

  // temp_*
  virtual Reading_base* readTemperature(Reading_base* reading = nullptr);

  // hum_*
  virtual Reading_base* readHumidity(Reading_base* reading = nullptr);

  // gas_*
  virtual Reading_base* readGas(Reading_base* reading = nullptr);

  // gas_lsb
  GET(heat_stab, bool);
  GET(gas_valid, bool);

  // idac_heat_x

  // res_heat_x

  // gas_wait_x

  // ctrl_gas_0
  ACCESSOR(heat_off, bool);

  // ctrl_gas_1
  ACCESSOR(heater_profile, uint8_t);
  ACCESSOR(run_gas, bool);

  // ctrl_hum
  enum class osrs_h : uint8_t {
    skipped			= 0x00,
    oversample_1		= 0x01,
    oversample_2		= 0x02,
    oversample_4		= 0x03,
    oversample_8		= 0x04,
    oversample_16		= 0x05,
    _reserved1			= 0x06,
    _reserved2			= 0x07,
  };

  ACCESSOR(H_oversample, osrs_h);
  ACCESSOR(spi_3w_int_en, bool);

  // status
  ACCESSOR(spi_mem_page, bool);

  // ctrl_meas
  enum class mode : uint8_t {
    sleep			= 0x00,
    forced			= 0x01,
    _reserved1			= 0x02,
    _reserved2			= 0x03,
  };

  enum class osrs_p : uint8_t {
    skipped			= 0x00,
    oversample_1		= 0x04,
    oversample_2		= 0x08,
    oversample_4		= 0x0c,
    oversample_8		= 0x10,
    oversample_16		= 0x14,
    _reserved1			= 0x18,
    _reserved2			= 0x1c,
  };

  enum class osrs_t : uint8_t {
    skipped			= 0x00,
    oversample_1		= 0x20,
    oversample_2		= 0x40,
    oversample_4		= 0x60,
    oversample_8		= 0x80,
    oversample_16		= 0xa0,
    _reserved1			= 0xc0,
    _reserved2			= 0xe0,
  };

  ACCESSOR(mode, mode);
  ACCESSOR(P_oversample, osrs_p);
  ACCESSOR(T_oversample, osrs_t);

  // config
  enum class filter : uint8_t {
    coefficient_0		= 0x00,
    coefficient_1		= 0x04,
    coefficient_3		= 0x08,
    coefficient_7		= 0x0c,
    coefficient_15		= 0x10,
    coefficient_31		= 0x14,
    coefficient_63		= 0x18,
    coefficient_127		= 0x1c,
  };

  ACCESSOR(spi_3w_en, bool);
  ACCESSOR(filter, filter);

  // others
  uint8_t chip_id(void);

  void reset(void);

}; // class BME680


class BME680Reading : public Reading_base {
protected:
  BME680 *_sensor;
  float _t_fine;
  bool _have_t_fine;

public:
  BME680Reading(BME680* s, uint32_t rt = 0x80000000, uint32_t rp = 0x80000000, uint32_t rh = 0x80000000, uint32_t rg = 0x80000000);

  virtual float temperature(void);
  virtual float pressure(void);
  virtual float humidity(void);
  virtual float gas(void);

}; // class BME680Reading

#undef GET
#undef SET
#undef ACCESSOR
