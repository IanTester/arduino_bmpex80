/*
BMP085.h - BMP085/BMP180/BMP185 class
Copyright (C) 2019 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include <stdint.h>
#include "internal/i2cdevice.h"
#include "internal/sensor.h"
#include "internal/reading.h"

#define BMP085_I2CADDR 0x77

class BMP085Reading;

class BMP085 : public I2Cdevice_base, public TempSensor_base, public PressSensor_base {
private:
  uint8_t _oversampling;
  uint16_t _oss_mult, _oss_scale;

  friend BMP085Reading;
  float _ac1, _ac2, _ac3, _ac4, _ac5, _ac6;
  float _b1, _b2, _mb, _mc, _md;

  unsigned long _time_t, _time_p;

  enum class Register : uint8_t {
    Cal_AC1             = 0xaa,  // R   Calibration data (16 bits)
    Cal_AC2             = 0xac,  // R   Calibration data (16 bits)
    Cal_AC3             = 0xae,  // R   Calibration data (16 bits)
    Cal_AC4             = 0xb0,  // R   Calibration data (16 bits)
    Cal_AC5             = 0xb2,  // R   Calibration data (16 bits)
    Cal_AC6             = 0xb4,  // R   Calibration data (16 bits)
    Cal_B1              = 0xb6,  // R   Calibration data (16 bits)
    Cal_B2              = 0xb8,  // R   Calibration data (16 bits)
    Cal_MB              = 0xba,  // R   Calibration data (16 bits)
    Cal_MC              = 0xbc,  // R   Calibration data (16 bits)
    Cal_MD              = 0xbe,  // R   Calibration data (16 bits)

    Check               = 0xd0,

    Control             = 0xf4,
    Data                = 0xf6,
    Data_XLSB           = 0xf8,
  };

  enum class Command : uint8_t {
    ReadTemperature     = 0x2e,
    ReadPressure        = 0x34,
  };

public:
  //! Empty constructor
  BMP085();

  enum class Mode : uint8_t {
    UltraLowPower,
    Standard,
    HighRes,
    UltraHighRes,
  };

  //! Begin using sensor, setting a mode
  bool begin(Mode m = Mode::UltraHighRes);  // by default go ultra-highres

  bool check(void);

  /*! Ask sensor to measure the temperature
    @return Number of milliseconds until data should be available
  */
  virtual unsigned long measureTemperature(void);

  //! Is the temperature reading triggered by measureTemperature() ready to read?
  virtual bool temperatureReadingReady(void);

  /*! Ask sensor to measure the air pressure
    @return Number of milliseconds until data should be available
  */
  virtual unsigned long measurePressure(void);

  //! Is the temperature reading triggered by measurePressure() ready to read?
  virtual bool pressureReadingReady(void);

  virtual Reading_base* readTemperature(Reading_base* reading = nullptr);
  virtual Reading_base* readPressure(Reading_base* reading = nullptr);

}; // class BMP085


//! Class for holding and converting readings from a BMP085/BMP180/BMP185
class BMP085Reading : public Reading_base {
protected:
  BMP085 *_sensor;

  float _b5;
  bool _have_b5;

  void _computeB5(void);

public:
  //! Constructor for temperature and air pressure
  BMP085Reading(BMP085* s, uint32_t rt = 0x80000000, uint32_t rp = 0x80000000);

  //! Convert the raw temperature reading into degrees Celcius
  virtual float temperature(void);

  //! Convert the raw air pressure reading into pascals
  virtual float pressure(void);

}; // class BMP085Reading

