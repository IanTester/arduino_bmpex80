/*
BME280.h - BME280 class
Copyright (C) 2019 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include "internal/BMP280.h"

#define GET(N, T) T get_##N(void)
#define SET(N, T) void set_##N(T val)
#define ACCESSOR(N, T) T get_##N(void);		\
  void set_##N(T val)

class BME280Reading;

class BME280 : public BMP280, public HumSensor_base {
protected:
  uint8_t _ctrl_hum, _old_ctrl_hum;

  friend BME280Reading;
  float _dig_h1, _dig_h2, _dig_h3, _dig_h4, _dig_h5, _dig_h6;

  // Unfortunately, enum classes look like classes
  // but can't inherit from other enum classes :(
  enum class Register : uint8_t {
    dig_T1		= 0x88,	// R/O unsigned short
    dig_T2		= 0x8a,	// R/O signed short
    dig_T3		= 0x8c,	// R/O signed short
    dig_P1		= 0x8e,	// R/O unsigned short
    dig_P2		= 0x90,	// R/O signed short
    dig_P3		= 0x92,	// R/O signed short
    dig_P4		= 0x94,	// R/O signed short
    dig_P5		= 0x96,	// R/O signed short
    dig_P6		= 0x98,	// R/O signed short
    dig_P7		= 0x9a,	// R/O signed short
    dig_P8		= 0x9c,	// R/O signed short
    dig_P9		= 0x9e,	// R/O signed short
    dig_H1		= 0xa1,	// R/O unsigned char

    chip_id		= 0xd0, // R/O

    reset		= 0xe0, // W/O
    dig_H2		= 0xe1,	// R/O signed short
    dig_H3		= 0xe3,	// R/O unsigned char
    dig_H4		= 0xe4,	// R/O signed short
    dig_H5		= 0xe5,	// R/O signed short
    dig_H6		= 0xe7,	// R/O signed char

    ctrl_hum		= 0xf2,	// R/W
    status		= 0xf3, // R/O
    ctrl_meas		= 0xf4, // R/W
    config		= 0xf5,	// R/W

    press_msb		= 0xf7,	// R/O
    press_lsb		= 0xf8,	// R/O
    press_xlsb		= 0xf9,	// R/O
    temp_msb		= 0xfa,	// R/O
    temp_lsb		= 0xfb,	// R/O
    temp_xlsb		= 0xfc,	// R/O
    hum_msb		= 0xfd,	// R/O
    hum_lsb		= 0xfe,	// R/O
  };

  enum class ctrl_hum_mask : uint8_t {
    osrs_h		= 0x03,
  };

  unsigned long _measurement_time(void);


public:
  //! Constructor
  BME280(uint8_t i2c_addr);

  //! Read calibration data and config registers
  bool begin(void);

  // ctrl_hum
  enum class osrs_h : uint8_t {
    skipped		= 0x00,
    oversample_1	= 0x01,
    oversample_2	= 0x02,
    oversample_4	= 0x03,
    oversample_8	= 0x04,
    oversample_16	= 0x05,
    _reserved1		= 0x06,
    _reserved2		= 0x07,
  };

  ACCESSOR(H_oversample, osrs_h);

  // config
  enum class t_standby : uint8_t {
    t0_5ms		= 0x00,
    t62_5ms		= 0x20,
    t125ms		= 0x40,
    t250ms		= 0x60,
    t500ms		= 0x80,
    t1000ms		= 0xa0,
    t10ms		= 0xc0,
    t20ms		= 0xe0,

    t1s			= 0xa0,
  };

  ACCESSOR(standby_time, t_standby);

  virtual unsigned long measureTemperature(void);
  virtual bool temperatureReadingReady(void);

  virtual unsigned long measurePressure(void);
  virtual bool pressureReadingReady(void);

  virtual unsigned long measureHumidity(void);
  virtual bool humidityReadingReady(void);

  // hum_*
  virtual Reading_base* readAll(void);
  virtual Reading_base* readHumidity(Reading_base* reading = nullptr);
  virtual Reading_base* readTemperature(Reading_base* reading = nullptr);
  virtual Reading_base* readPressure(Reading_base* reading = nullptr);

}; // class BME280


class BME280Reading : public BMP280Reading {
protected:
  BME280 *_sensor;
  float _t_fine;
  bool _have_t_fine;

public:
  //! Constructor for all readings
  BME280Reading(BME280* s, uint32_t rt = 0x80000000, uint32_t rp = 0x80000000, uint32_t rh = 0x80000000);

  virtual float humidity(void);

}; // class Reading

#undef GET
#undef SET
#undef ACCESSOR
