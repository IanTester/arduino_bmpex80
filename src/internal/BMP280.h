/*
BMP280.h - BMP280 class
Copyright (C) 2019 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include <stdint.h>
#include "internal/i2cdevice.h"
#include "internal/sensor.h"
#include "internal/reading.h"

#define GET(N, T) T get_##N(void)
#define SET(N, T) void set_##N(T val)
#define ACCESSOR(N, T) T get_##N(void);		\
  void set_##N(T val)

class BMP280Reading;

class BMP280 : public I2Cdevice_base, public TempSensor_base, public PressSensor_base {
protected:
  uint8_t _ctrl_meas, _config, _old_ctrl_meas;

  friend BMP280Reading;
  float _dig_t1, _dig_t2, _dig_t3;
  float _dig_p1, _dig_p2, _dig_p3, _dig_p4, _dig_p5, _dig_p6, _dig_p7, _dig_p8, _dig_p9;

  enum class Register : uint8_t {
    dig_T1		= 0x88,	// R/O unsigned short
    dig_T2		= 0x8a,	// R/O signed short
    dig_T3		= 0x8c,	// R/O signed short
    dig_P1		= 0x8e,	// R/O unsigned short
    dig_P2		= 0x90,	// R/O signed short
    dig_P3		= 0x92,	// R/O signed short
    dig_P4		= 0x94,	// R/O signed short
    dig_P5		= 0x96,	// R/O signed short
    dig_P6		= 0x98,	// R/O signed short
    dig_P7		= 0x9a,	// R/O signed short
    dig_P8		= 0x9c,	// R/O signed short
    dig_P9		= 0x9e,	// R/O signed short
    // 0xa0 and 0xa1 are registered

    chip_id		= 0xd0, // R/O

    reset		= 0xe0, // W/O

    status		= 0xf3, // R/O
    ctrl_meas		= 0xf4, // R/W
    config		= 0xf5,	// R/W

    press_msb		= 0xf7,	// R/O
    press_lsb		= 0xf8,	// R/O
    press_xlsb		= 0xf9,	// R/O
    temp_msb		= 0xfa,	// R/O
    temp_lsb		= 0xfb,	// R/O
    temp_xlsb		= 0xfc,	// R/O
  };

  enum class status_mask : uint8_t {
    im_update		= 0x01,
    measuring		= 0x08,
  };

  enum class ctrl_meas_mask : uint8_t {
    mode		= 0x03,
    osrs_p		= 0x1c,
    osrs_t		= 0xe0,
  };

  enum class config_mask : uint8_t {
    spi3w_en		= 0x01,
    filter		= 0x1c,
    t_standby		= 0xe0,
  };

  unsigned long _measurement_time(void);

public:
  //! Constructor
  BMP280(uint8_t i2c_addr);

  //! Read calibration data and config registers
  bool begin(void);

  uint8_t chip_id(void);

  void reset(void);

  // status
  GET(im_update, bool);
  GET(measuring, bool);

  // ctrl_meas
  enum class mode : uint8_t {
    sleep		= 0x00,
    forced		= 0x01,
    forced2		= 0x02,	// same as 'forced'
    normal		= 0x03,
  };

  enum class osrs_p : uint8_t {
    skipped		= 0x00,
    oversample_1	= 0x04,
    oversample_2	= 0x08,
    oversample_4	= 0x0c,
    oversample_8	= 0x10,
    oversample_16	= 0x14,
    _reserved1		= 0x18,
    _reserved2		= 0x1c,
  };

  enum class osrs_t : uint8_t {
    skipped		= 0x00,
    oversample_1	= 0x20,
    oversample_2	= 0x40,
    oversample_4	= 0x60,
    oversample_8	= 0x80,
    oversample_16	= 0xa0,
    _reserved1		= 0xc0,
    _reserved2		= 0xe0,
  };

  ACCESSOR(mode, mode);
  ACCESSOR(P_oversample, osrs_p);
  ACCESSOR(T_oversample, osrs_t);
  void write_ctrl_meas(void);

  // config
  enum class filter : uint8_t {
    off			= 0x00,
    coefficient_2	= 0x04,
    coefficient_4	= 0x08,
    coefficient_8	= 0x0c,
    coefficient_16	= 0x10,
    _reserved1		= 0x14,
    _reserved2		= 0x18,
    _reserved3		= 0x1c,
  };

  enum class t_standby : uint8_t {
    t0_5ms		= 0x00,
    t62_5ms		= 0x20,
    t125ms		= 0x40,
    t250ms		= 0x60,
    t500ms		= 0x80,
    t1000ms		= 0xa0,
    t2000ms		= 0xc0,
    t4000ms		= 0xe0,

    t1s			= 0xa0,
    t2s			= 0xc0,
    t4s			= 0xe0,
  };

  ACCESSOR(spi3w_en, bool);
  ACCESSOR(filter, filter);
  ACCESSOR(standby_time, t_standby);
  void write_config(void);

  inline virtual bool can_MeasureAll(void) { return true; }

  virtual unsigned long measureAll(void);
  virtual bool allReadingsReady(void);

  virtual unsigned long measureTemperature(void);
  virtual bool temperatureReadingReady(void);

  virtual unsigned long measurePressure(void);
  virtual bool pressureReadingReady(void);

  virtual Reading_base* readAll(void);
  virtual Reading_base* readTemperature(Reading_base* reading = nullptr);
  virtual Reading_base* readPressure(Reading_base* reading = nullptr);

}; // class BMP280


class BMP280Reading : public Reading_base {
protected:
  BMP280 *_sensor;
  float _t_fine;
  bool _have_t_fine;

public:
  //! Constructor for temperature and pressure
  BMP280Reading(BMP280* s, uint32_t rt = 0x80000000, uint32_t rp = 0x80000000);

  virtual float temperature(void);
  virtual float pressure(void);

}; // class BMP280Reading


#undef GET
#undef SET
#undef ACCESSOR
