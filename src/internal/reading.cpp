/*
BMPEx80/src/internal/reading.cpp - base class for sensor reading
Copyright (C) 2019-2020 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "internal/reading.h"

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include <Wire.h>

float Reading_base::temperature(void) {
  return -9999;
}

float Reading_base::pressure(void) {
  return -9999;
}

float Reading_base::humidity(void) {
  return -9999;
}

float Reading_base::gas(void) {
  return -9999;
}

float Reading_base::sealevelPressure(float altitude) {
  return pressure() / pow(1 - altitude / 44330, 5.255);
}

float Reading_base::altitude(float sealevelPressure) {
  return 44330 * (1 - pow(pressure() / sealevelPressure, 0.1903));
}
