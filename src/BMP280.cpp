/*
BMP280.cpp - BMP280 class
Copyright (C) 2019 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "internal/common.h"
#include "internal/BMP280.h"

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include <Wire.h>

BMP280::BMP280(uint8_t i2c_addr) :
  I2Cdevice_base(i2c_addr),
  TempSensor_base(),
  PressSensor_base(),
  _ctrl_meas(0),
  _config(0)
{}

bool BMP280::begin(void) {
  _dig_t1 = (float)_read<uint16_t>(Register::dig_T1) / 8192.0;
  if (_i2c_error) return false;
  _dig_t2 = (float)_read<int16_t>(Register::dig_T2) * 8.0;
  if (_i2c_error) return false;
  _dig_t3 = (float)_read<int16_t>(Register::dig_T3);
  if (_i2c_error) return false;

  _dig_p1 = (float)_read<uint16_t>(Register::dig_P1);
  if (_i2c_error) return false;
  _dig_p2 = (float)_read<int16_t>(Register::dig_P2);
  if (_i2c_error) return false;
  _dig_p3 = (float)_read<int16_t>(Register::dig_P3) / 524288.0;
  if (_i2c_error) return false;
  _dig_p4 = (float)_read<int16_t>(Register::dig_P4) * 65536.0;
  if (_i2c_error) return false;
  _dig_p5 = (float)_read<int16_t>(Register::dig_P5) * 2.0;
  if (_i2c_error) return false;
  _dig_p6 = (float)_read<int16_t>(Register::dig_P6) / 32768.0;
  if (_i2c_error) return false;
  _dig_p7 = (float)_read<int16_t>(Register::dig_P7);
  if (_i2c_error) return false;
  _dig_p8 = (float)_read<int16_t>(Register::dig_P8) / 32768.0;
  if (_i2c_error) return false;
  _dig_p9 = (float)_read<int16_t>(Register::dig_P9) / 2147483648.0;
  if (_i2c_error) return false;

  _ctrl_meas = _read<uint8_t>(Register::ctrl_meas);
  _config = _read<uint8_t>(Register::config);

  return true;
}

uint8_t BMP280::chip_id(void) {
  return _read<uint8_t>(Register::chip_id);
}

void BMP280::reset(void) {
  _write(Register::reset, 0x86);
  _ctrl_meas = _config = 0;
}

#define GET(N, T, V, M) T BMP280::get_##N(void) { return static_cast<T>(V & static_cast<uint8_t>(M)); }
#define SET(N, T, V, M) void BMP280::set_##N(T val) {\
    V &= ~static_cast<uint8_t>(M);		    \
    V |= static_cast<uint8_t>(val);		    \
  }
#define SET_BOOL(N, V, M) void BMP280::set_##N(bool val) {		\
    if (val) {								\
      V |= static_cast<uint8_t>(M);					\
    } else {								\
      V &= ~static_cast<uint8_t>(M);					\
    }									\
  }

// status

GET(im_update, bool, _read<uint8_t>(Register::status), status_mask::im_update);
GET(measuring, bool, _read<uint8_t>(Register::status), status_mask::measuring);

// ctrl_meas

GET(mode, BMP280::mode, _ctrl_meas, ctrl_meas_mask::mode);
SET(mode, mode, _ctrl_meas, ctrl_meas_mask::mode);

GET(P_oversample, BMP280::osrs_p, _ctrl_meas, ctrl_meas_mask::osrs_p);
SET(P_oversample, osrs_p, _ctrl_meas, ctrl_meas_mask::osrs_p);

GET(T_oversample, BMP280::osrs_t, _ctrl_meas, ctrl_meas_mask::osrs_t);
SET(T_oversample, osrs_t, _ctrl_meas, ctrl_meas_mask::osrs_t);

void BMP280::write_ctrl_meas(void) {
  _write(Register::ctrl_meas, _ctrl_meas);
}

// config

GET(spi3w_en, bool, _config, config_mask::spi3w_en);
SET_BOOL(spi3w_en, _config, config_mask::spi3w_en);

GET(filter, BMP280::filter, _config, config_mask::filter);
SET(filter, filter, _config, config_mask::filter);

GET(standby_time, BMP280::t_standby, _config, config_mask::t_standby);
SET(standby_time, t_standby, _config, config_mask::t_standby);

void BMP280::write_config(void) {
  _write(Register::config, _config);
}

// measurements

unsigned long BMP280::_measurement_time(void) {
  uint8_t osrs_p = static_cast<uint8_t>(get_P_oversample()) >> 2;
  uint8_t p = (osrs_p == 0 ? 0 : 1 << (osrs_p - 1));

  uint8_t osrs_t = static_cast<uint8_t>(get_T_oversample()) >> 5;
  uint8_t t = (osrs_t == 0 ? 0 : 1 << (osrs_t - 1));

  return (2000 * t) + (2000 * p) + 1500;
}

unsigned long BMP280::measureAll(void) {
  set_mode(mode::forced);

  return _measurement_time();
}

bool BMP280::allReadingsReady(void) {
  if (get_measuring())
    return false;

  _ctrl_meas = _read<uint8_t>(Register::ctrl_meas);
  return true;
}

unsigned long BMP280::measureTemperature(void) {
  _old_ctrl_meas = _ctrl_meas;
  set_P_oversample(osrs_p::skipped);
  set_mode(mode::forced);

  return _measurement_time();
}

bool BMP280::temperatureReadingReady(void) {
  if (get_measuring())
    return false;

  _ctrl_meas = _read<uint8_t>(Register::ctrl_meas);
  set_P_oversample(static_cast<osrs_p>(_old_ctrl_meas & static_cast<uint8_t>(ctrl_meas_mask::osrs_p)));
  return true;
}

unsigned long BMP280::measurePressure(void) {
  _old_ctrl_meas = _ctrl_meas;
  set_T_oversample(osrs_t::skipped);
  set_mode(mode::forced);

  return _measurement_time();
}

bool BMP280::pressureReadingReady(void) {
  if (get_measuring())
    return false;

  _ctrl_meas = _read<uint8_t>(Register::ctrl_meas);
  set_T_oversample(static_cast<osrs_t>(_old_ctrl_meas & static_cast<uint8_t>(ctrl_meas_mask::osrs_t)));
  return true;
}

Reading_base* BMP280::readAll(void) {
  uint8_t data[6];
  if (!_read(Register::press_msb, data, 6))
    return nullptr;

  uint32_t rp = ((uint32_t)data[0] << 12) | ((uint32_t)data[1] << 4) | (data[2] >> 4);
  uint32_t rt = ((uint32_t)data[3] << 12) | ((uint32_t)data[4] << 4) | (data[5] >> 4);

  return (Reading_base*)(new BMP280Reading(this, rt, rp));
}

Reading_base* BMP280::readTemperature(Reading_base* reading) {
  uint8_t data[3];
  if (!_read(Register::temp_msb, data, 3))
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BMP280Reading(this));
  reading->_set_raw_temp(((uint32_t)data[0] << 12) | ((uint32_t)data[1] << 4) | (data[2] >> 4));

  return reading;
}

Reading_base* BMP280::readPressure(Reading_base* reading) {
  uint8_t data[3];
  if (!_read(Register::press_msb, data, 3))
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BMP280Reading(this));
  reading->_set_raw_press(((uint32_t)data[0] << 12) | ((uint32_t)data[1] << 4) | (data[2] >> 4));

  return reading;
}


BMP280Reading::BMP280Reading(BMP280* s, uint32_t rt, uint32_t rp) :
  Reading_base(rt, rp),
  _sensor(s),
  _have_t_fine(false)
{}

float BMP280Reading::temperature(void) {
  float t = (float)_rt / 131072.0;

  float var1 = (t - _sensor->_dig_t1) * _sensor->_dig_t2;
  float var2 = sqr(t - _sensor->_dig_t1) * _sensor->_dig_t3;
  _t_fine = var1 + var2;
  _have_t_fine = true;

  return _t_fine / 5120.0;
}

float BMP280Reading::pressure(void) {
  if (!_have_t_fine)
    temperature();

  float var1 = (_t_fine / 2.0) - 64000.0;
  float var2 = sqr(var1) * _sensor->_dig_p6;
  var2 += var1 * _sensor->_dig_p5;
  var2 = (var2 * 0.25) + _sensor->_dig_p4;
  var1 = ((_sensor->_dig_p3 * sqr(var1)) + _sensor->_dig_p2) * var1 / 524288.0;
  var1 = (1.0 + (var1 / 32768.0)) * _sensor->_dig_p1;
  if (fabs(var1) < 1e-6) {
    return 0; // avoid exception caused by division by zero
  }

  float p = 1048576.0 - (float)_rp;
  p = (p - (var2 / 4096.0)) * 6250.0 / var1;
  var1 = _sensor->_dig_p9 * sqr(p);
  var2 = p * _sensor->_dig_p8;

  return p + (var1 + var2 + _sensor->_dig_p7) / 16.0;
}
