/*
BME680.cpp - BME680 class
Copyright (C) 2019 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "internal/common.h"
#include "internal/BME680.h"

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include <Wire.h>

BME680::BME680(uint8_t i2c_addr) :
  I2Cdevice_base(i2c_addr),
  TempSensor_base(),
  PressSensor_base(),
  HumSensor_base(),
  GasSensor_base(),
  _ctrl_gas_0(0),
  _ctrl_gas_1(0),
  _ctrl_hum(0),
  _ctrl_meas(0),
  _config(0),
  _status(1)
{
}

bool BME680::begin(void) {
  _res_heat_val = _read<int8_t>(Register::res_heat_val);
  if (_i2c_error) return false;
  _res_heat_range = _read<uint8_t>(Register::res_heat_range) & static_cast<uint8_t>(res_heat_range_mask::res_heat_range);
  if (_i2c_error) return false;
  _range_sw_err = _read<int8_t>(Register::range_sw_err) & static_cast<uint8_t>(range_sw_err_mask::range_sw_err);
  if (_i2c_error) return false;

  _par_t1 = (float)_read<uint16_t>(Register::par_T1) / 1024.0;
  if (_i2c_error) return false;
  _par_t2 = (float)_read<int16_t>(Register::par_T2);
  if (_i2c_error) return false;
  _par_t3 = (float)_read<int8_t>(Register::par_T3) * 16.0;
  if (_i2c_error) return false;

  _par_p1 = (float)_read<uint16_t>(Register::par_P1);
  if (_i2c_error) return false;
  _par_p2 = (float)_read<int16_t>(Register::par_P2);
  if (_i2c_error) return false;
  _par_p3 = (float)_read<int8_t>(Register::par_P3) / 16384.0;
  if (_i2c_error) return false;
  _par_p4 = (float)_read<int16_t>(Register::par_P4) * 65536.0;
  if (_i2c_error) return false;
  _par_p5 = (float)_read<int16_t>(Register::par_P5) * 2.0;
  if (_i2c_error) return false;
  _par_p6 = (float)_read<int8_t>(Register::par_P6) / 131072.0;
  if (_i2c_error) return false;
  _par_p7 = (float)_read<int8_t>(Register::par_P7) * 128.0;
  if (_i2c_error) return false;
  _par_p8 = (float)_read<int16_t>(Register::par_P8) / 32768.0;
  if (_i2c_error) return false;
  _par_p9 = (float)_read<int16_t>(Register::par_P9) / 2147483648.0;
  if (_i2c_error) return false;
  _par_p10 = (float)_read<uint8_t>(Register::par_P10);
  if (_i2c_error) return false;

  _par_h1 = (float)_read<uint16_t>(Register::par_H1) * 16.0;
  if (_i2c_error) return false;
  {
    uint16_t temp = _read<uint16_t>(Register::par_H2);
    _par_h2 = (float)(((temp & 0x00ff) << 8) | ((temp & 0xff00) >> 8)) / 262144.0;
  }
  if (_i2c_error) return false;
  _par_h3 = (float)_read<int8_t>(Register::par_H3) / 2.0;
  if (_i2c_error) return false;
  _par_h4 = (float)_read<int8_t>(Register::par_H4) / 16384.0;
  if (_i2c_error) return false;
  _par_h5 = (float)_read<int8_t>(Register::par_H5) / 1048576.0;
  if (_i2c_error) return false;
  _par_h6 = (float)_read<uint8_t>(Register::par_H6) / 16384.0;
  if (_i2c_error) return false;
  _par_h7 = (float)_read<int8_t>(Register::par_H7) / 2097152.0;
  if (_i2c_error) return false;

  _par_gh1 = (float)_read<int8_t>(Register::par_GH1) / 16.0;
  if (_i2c_error) return false;
  _par_gh2 = (float)_read<int16_t>(Register::par_GH2) / 32768.0;
  if (_i2c_error) return false;
  _par_gh3 = (float)_read<int8_t>(Register::par_GH3) / 1024.0;
  if (_i2c_error) return false;

}

#define GET(N, T, V, M) T BME680::get_##N(void) { return static_cast<T>(V & static_cast<uint8_t>(M)); }
#define SET(N, T, V, M) void BME680::set_##N(T val) {\
    V &= ~static_cast<uint8_t>(M);		    \
    V |= static_cast<uint8_t>(val);		    \
  }
#define SET_BOOL(N, V, M) void BME680::set_##N(bool val) {		\
    if (val) {								\
      V |= static_cast<uint8_t>(M);					\
    } else {								\
      V &= ~static_cast<uint8_t>(M);					\
    }									\
  }


// eas_status_0
GET(gas_measurement_index, uint8_t, _read<uint8_t>(Register::eas_status_0), eas_status_0_mask::gas_meas_index_0);
GET(measuring, bool, _read<uint8_t>(Register::eas_status_0), eas_status_0_mask::measuring);
GET(gas_measuring, bool, _read<uint8_t>(Register::eas_status_0), eas_status_0_mask::gas_measuring);
GET(new_data_0, bool, _read<uint8_t>(Register::eas_status_0), eas_status_0_mask::new_data_0);

unsigned long BME680::measureAll(void) {
  set_mode(mode::forced);

  return 10;
}

bool BME680::allReadingsReady(void) {
  if (get_measuring())
    return false;

  _ctrl_meas = _read<uint8_t>(Register::ctrl_meas);
  return true;
}

unsigned long BME680::measurePressure(void) {
  _old_ctrl_meas = _ctrl_meas;
  _old_ctrl_hum = _ctrl_hum;
  set_T_oversample(osrs_t::skipped);
  set_H_oversample(osrs_h::skipped);
  set_mode(mode::forced);

  return 10;
}

bool BME680::pressureReadingReady(void) {
  if (get_measuring())
    return false;

  _ctrl_meas = _read<uint8_t>(Register::ctrl_meas);
  set_T_oversample(static_cast<osrs_t>(_old_ctrl_meas & static_cast<uint8_t>(ctrl_meas_mask::osrs_t)));

  _ctrl_hum = _read<uint8_t>(Register::ctrl_hum);
  set_H_oversample(static_cast<osrs_h>(_old_ctrl_hum & static_cast<uint8_t>(ctrl_hum_mask::osrs_h)));

  return true;
}

unsigned long BME680::measureTemperature(void) {
  _old_ctrl_meas = _ctrl_meas;
  _old_ctrl_hum = _ctrl_hum;
  set_P_oversample(osrs_p::skipped);
  set_H_oversample(osrs_h::skipped);
  set_mode(mode::forced);

  return 10;
}

bool BME680::temperatureReadingReady(void) {
  if (get_measuring())
    return false;

  _ctrl_meas = _read<uint8_t>(Register::ctrl_meas);
  set_P_oversample(static_cast<osrs_p>(_old_ctrl_meas & static_cast<uint8_t>(ctrl_meas_mask::osrs_p)));

  _ctrl_hum = _read<uint8_t>(Register::ctrl_hum);
  set_H_oversample(static_cast<osrs_h>(_old_ctrl_hum & static_cast<uint8_t>(ctrl_hum_mask::osrs_h)));

  return true;
}

unsigned long BME680::measureHumidity(void) {
  _old_ctrl_meas = _ctrl_meas;
  set_T_oversample(osrs_t::skipped);
  set_P_oversample(osrs_p::skipped);

  set_mode(mode::forced);

  return 10;
}

bool BME680::humidityReadingReady(void) {
  if (get_measuring())
    return false;

  _ctrl_meas = _read<uint8_t>(Register::ctrl_meas);
  set_T_oversample(static_cast<osrs_t>(_old_ctrl_meas & static_cast<uint8_t>(ctrl_meas_mask::osrs_t)));
  set_P_oversample(static_cast<osrs_p>(_old_ctrl_meas & static_cast<uint8_t>(ctrl_meas_mask::osrs_p)));
  return true;
}

unsigned long BME680::measureGas(void) {
  _old_ctrl_meas = _ctrl_meas;
  _old_ctrl_hum = _ctrl_hum;
  set_P_oversample(osrs_p::skipped);
  set_T_oversample(osrs_t::skipped);
  set_H_oversample(osrs_h::skipped);

  set_mode(mode::forced);

  return 10;
}

bool BME680::gasReadingReady(void) {
  if (get_gas_measuring())
    return false;

  _ctrl_meas = _read<uint8_t>(Register::ctrl_meas);
  set_T_oversample(static_cast<osrs_t>(_old_ctrl_meas & static_cast<uint8_t>(ctrl_meas_mask::osrs_t)));
  set_P_oversample(static_cast<osrs_p>(_old_ctrl_meas & static_cast<uint8_t>(ctrl_meas_mask::osrs_p)));

  _ctrl_hum = _read<uint8_t>(Register::ctrl_hum);
  set_H_oversample(static_cast<osrs_h>(_old_ctrl_hum & static_cast<uint8_t>(ctrl_hum_mask::osrs_h)));

  return true;
}

Reading_base* BME680::readAll(void) {
  uint8_t data[10];
  if (!_read(Register::press_msb, data, 10))
    return nullptr;

  uint32_t rp = ((uint32_t)data[0] << 12) | ((uint32_t)data[1] << 4) | (data[2] >> 4);
  uint32_t rt = ((uint32_t)data[3] << 12) | ((uint32_t)data[4] << 4) | (data[5] >> 4);
  uint32_t rh = ((uint32_t)data[6] << 8) | data[7];
  uint32_t rg = ((uint32_t)data[8] << 8) | data[9];

  return (Reading_base*)(new BME680Reading(this, rt, rp, rh, rg));
}

Reading_base* BME680::readTemperature(Reading_base* reading) {
  uint8_t data[3];
  if (!_read(Register::temp_msb, data, 3))
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BME680Reading(this));
  reading->_set_raw_temp(((uint32_t)data[0] << 12) | ((uint32_t)data[1] << 4) | (data[2] >> 4));

  return reading;
}

Reading_base* BME680::readPressure(Reading_base* reading) {
  uint8_t data[3];
  if (!_read(Register::press_msb, data, 3))
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BME680Reading(this));
  reading->_set_raw_press(((uint32_t)data[0] << 12) | ((uint32_t)data[1] << 4) | (data[2] >> 4));

  return reading;
}

Reading_base* BME680::readHumidity(Reading_base* reading) {
  uint16_t rh = _read<uint16_t>(Register::hum_msb);
  if (_i2c_error)
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BME680Reading(this));
  reading->_set_raw_hum(rh);

  return reading;
}

Reading_base* BME680::readGas(Reading_base* reading) {
  uint16_t rg = _read<uint16_t>(Register::gas_msb);
  if (_i2c_error)
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BME680Reading(this));
  reading->_set_raw_gas(rg);

  return reading;
}

// gas_lsb
GET(heat_stab, bool, _read<uint8_t>(Register::gas_lsb), gas_lsb_mask::heat_stab);
GET(gas_valid, bool, _read<uint8_t>(Register::gas_lsb), gas_lsb_mask::gas_valid);

// ctrl_gas_0
GET(heat_off, bool, _ctrl_gas_0, ctrl_gas_0_mask::heat_off);
SET_BOOL(heat_off, _ctrl_gas_0, ctrl_gas_0_mask::heat_off);

// ctrl_gas_1
GET(heater_profile, uint8_t, _ctrl_gas_1, ctrl_gas_1_mask::nb_conv);
SET(heater_profile, uint8_t, _ctrl_gas_1, ctrl_gas_1_mask::nb_conv);
GET(run_gas, bool, _ctrl_gas_1, ctrl_gas_1_mask::run_gas);
SET_BOOL(run_gas, _ctrl_gas_1, ctrl_gas_1_mask::run_gas);

// ctrl_hum
GET(H_oversample, BME680::osrs_h, _ctrl_hum, ctrl_hum_mask::osrs_h);
SET(H_oversample, osrs_h, _ctrl_hum, ctrl_hum_mask::osrs_h);
GET(spi_3w_int_en, bool, _ctrl_hum, ctrl_hum_mask::spi_w3_int_en);
SET_BOOL(spi_3w_int_en, _ctrl_hum, ctrl_hum_mask::spi_w3_int_en);

// status
GET(spi_mem_page, bool, _status, status_mask::spi_mem_page);
SET_BOOL(spi_mem_page, _status, status_mask::spi_mem_page);

// ctrl_meas
GET(mode, BME680::mode, _ctrl_meas, ctrl_meas_mask::mode);
SET(mode, mode, _ctrl_meas, ctrl_meas_mask::mode);
GET(P_oversample, BME680::osrs_p, _ctrl_meas, ctrl_meas_mask::osrs_p);
SET(P_oversample, osrs_p, _ctrl_meas, ctrl_meas_mask::osrs_p);
GET(T_oversample, BME680::osrs_t, _ctrl_meas, ctrl_meas_mask::osrs_t);
SET(T_oversample, osrs_t, _ctrl_meas, ctrl_meas_mask::osrs_t);

// config
GET(spi_3w_en, bool, _config, config_mask::spi_3w_en);
SET_BOOL(spi_3w_en, _config, config_mask::spi_3w_en);
GET(filter, BME680::filter, _config, config_mask::filter);
SET(filter, filter, _config, config_mask::filter);

uint8_t BME680::chip_id(void) {
  return _read<uint8_t>(Register::id);
}

void BME680::reset(void) {
  _write(Register::reset, 0xb6);
}


BME680Reading::BME680Reading(BME680* s, uint32_t rt, uint32_t rp, uint32_t rh, uint32_t rg) :
  Reading_base(rt, rp, rh, rg),
  _sensor(s),
  _have_t_fine(false)
{
}

float BME680Reading::temperature(void) {
  float var1  = ((_rt / 16384.0f) - _sensor->_par_t1) * _sensor->_par_t2;
  float var2  = sqr(((_rt / 131072.0f) - (_sensor->_par_t1 / 8))) * _sensor->_par_t3;

  _t_fine = var1 + var2;
  _have_t_fine = true;

  return _t_fine / 512.0;
}

float BME680Reading::pressure(void) {
  if (!_have_t_fine)
    temperature();

  float var1 = (_t_fine / 2.0f) - 64000.0f;
  float var2 = var1 * var1 * _sensor->_par_p6;
  var2 = var2 + (var1 * _sensor->_par_p5);
  var2 = (var2 / 4.0f) + _sensor->_par_p4;
  var1 = ((_sensor->_par_p3 * sqr(var1)) + (_sensor->_par_p2 * var1)) / 524288.0;
  var1 = (1.0f + (var1 / 32768.0f)) * _sensor->_par_p1;

  // Avoid exception caused by division by zero
  if (fabs(var1) < 1e-9)
    return 0;

  float calc_pres = ((((1048576.0f - _rp) - (var2 / 4096.0f)) * 6250.0f) / var1);
  var1 = _sensor->_par_p9 * sqr(calc_pres);
  var2 = calc_pres * _sensor->_par_p8;
  float var3 = ((calc_pres / 256.0f) * (calc_pres / 256.0f) * (calc_pres / 256.0f)
		* (_sensor->_par_p10 / 131072.0f));
  return (calc_pres + (var1 + var2 + var3 + _sensor->_par_p7) / 16.0f);
}

float BME680Reading::humidity(void) {
  if (!_have_t_fine)
    temperature();

  float temp_comp  = _t_fine / 5120.0f;
  float var1 = _rh - (_sensor->_par_h1 + (_sensor->_par_h3 * temp_comp));
  float var2 = var1 * _sensor->_par_h2 * (1.0f + (_sensor->_par_h4 * temp_comp) + (_sensor->_par_h5 * sqr(temp_comp)));

  return var2 + (_sensor->_par_h6 + (_sensor->_par_h7 * temp_comp)) * sqr(var2);
}

float BME680Reading::gas(void) {
  const float lookup_k1_range[16] = { 0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, -0.8,  0.0, 0.0, -0.2, -0.5, 0.0, -1.0, 0.0, 0.0 };
  const float lookup_k2_range[16] = { 0.0, 0.0, 0.0, 0.0, 0.1,  0.7, 0.0, -0.8, -0.1, 0.0,  0.0,  0.0, 0.0,  0.0, 0.0, 0.0 };

  uint16_t gas_res = ((_rg & 0x00ff) << 2) | ((_rg & 0xc000) >> 14);
  uint8_t gas_range = (_rg & 0x0f00) >> 8;

  float var1 = 340.0f + (5.0f * _sensor->_range_sw_err);
  float var2 = var1 * (1.0f + lookup_k1_range[gas_range] / 100.0f);
  float var3 = 1.0f + (lookup_k2_range[gas_range]/100.0f);

  return 1.0f / (var3 * 0.000000125 * (float)(1 << gas_range) * (((gas_res - 512.0f) / var2) + 1.0f));
}

