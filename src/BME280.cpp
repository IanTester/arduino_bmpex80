/*
BME280.cpp - BME280 class
Copyright (C) 2019 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "internal/BME280.h"

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include <Wire.h>

BME280::BME280(uint8_t i2c_addr) :
  BMP280(i2c_addr),
  HumSensor_base()
{}

bool BME280::begin(void) {
  BMP280::begin();

  _dig_h1 = (float)_read<uint8_t>(Register::dig_H1) / 524288.0;
  if (_i2c_error) return false;
  _dig_h2 = (float)_read<int16_t>(Register::dig_H2) / 65536.0;
  if (_i2c_error) return false;
  _dig_h3 = (float)_read<uint8_t>(Register::dig_H3) / 67108864.0;
  if (_i2c_error) return false;

  uint8_t data[3];
  if (!_read(Register::dig_H4, data, 3)) return false;

  int16_t h4 = ((uint16_t)data[0] << 4) | (data[1] & 0x0f);
  _dig_h4 = (float)h4 * 64.0;

  int16_t h5 = ((uint16_t)data[2] << 4) | (data[1] >> 4);
  _dig_h5 = (float)h5 / 16384.0;

  _dig_h6 = (float)_read<int8_t>(Register::dig_H6) / 67108864.0;
  if (_i2c_error) return false;

  _ctrl_hum = _read<uint8_t>(Register::ctrl_hum);
}

// ctrl_hum

BME280::osrs_h BME280::get_H_oversample(void) {
  return static_cast<osrs_h>(_ctrl_hum & static_cast<uint8_t>(ctrl_hum_mask::osrs_h));
}

void BME280::set_H_oversample(osrs_h oh) {
  _ctrl_hum &= ~static_cast<uint8_t>(ctrl_hum_mask::osrs_h);
  _ctrl_hum |= static_cast<uint8_t>(oh);
  _write(Register::ctrl_hum, _ctrl_hum);
}

// Measure 

unsigned long BME280::_measurement_time(void) {
  uint8_t osrs_p = static_cast<uint8_t>(get_P_oversample()) >> 2;
  uint8_t t_p = (osrs_p == 0 ? 0 : 500 + (2000 * (1 << (osrs_p - 1))));

  uint8_t osrs_t = static_cast<uint8_t>(get_T_oversample()) >> 5;
  uint8_t t_t = (osrs_t == 0 ? 0 : 2000 * (1 << (osrs_t - 1)));

  uint8_t osrs_h = static_cast<uint8_t>(get_H_oversample());
  uint8_t t_h = (osrs_t == 0 ? 0 : 500 + (2000 * (1 << (osrs_h - 1))));

  return t_t + t_p + t_h + 1000;
}

unsigned long BME280::measureTemperature(void) {
  _old_ctrl_meas = _ctrl_meas;
  set_P_oversample(osrs_p::skipped);

  _old_ctrl_hum = _ctrl_hum;
  set_H_oversample(osrs_h::skipped);

  set_mode(mode::forced);

  return _measurement_time();
}

bool BME280::temperatureReadingReady(void) {
  if (get_measuring())
    return false;

  _ctrl_meas = _read<uint8_t>(Register::ctrl_meas);
  set_P_oversample(static_cast<osrs_p>(_old_ctrl_meas & static_cast<uint8_t>(ctrl_meas_mask::osrs_p)));

  _ctrl_hum = _read<uint8_t>(Register::ctrl_hum);
  set_H_oversample(static_cast<osrs_h>(_old_ctrl_hum & static_cast<uint8_t>(ctrl_hum_mask::osrs_h)));

  return true;
}

unsigned long BME280::measurePressure(void) {
  _old_ctrl_meas = _ctrl_meas;
  set_T_oversample(osrs_t::skipped);

  _old_ctrl_hum = _ctrl_hum;
  set_H_oversample(osrs_h::skipped);

  set_mode(mode::forced);

  return _measurement_time();
}

bool BME280::pressureReadingReady(void) {
  if (get_measuring())
    return false;

  _ctrl_meas = _read<uint8_t>(Register::ctrl_meas);
  set_T_oversample(static_cast<osrs_t>(_old_ctrl_meas & static_cast<uint8_t>(ctrl_meas_mask::osrs_t)));

  _ctrl_hum = _read<uint8_t>(Register::ctrl_hum);
  set_H_oversample(static_cast<osrs_h>(_old_ctrl_hum & static_cast<uint8_t>(ctrl_hum_mask::osrs_h)));

  return true;
}

unsigned long BME280::measureHumidity(void) {
  _old_ctrl_meas = _ctrl_meas;
  set_P_oversample(osrs_p::skipped);
  set_T_oversample(osrs_t::skipped);

  set_mode(mode::forced);

  return _measurement_time();
}

bool BME280::humidityReadingReady(void) {
  if (get_measuring())
    return false;

  _ctrl_meas = _read<uint8_t>(Register::ctrl_meas);
  set_T_oversample(static_cast<osrs_t>(_old_ctrl_meas & static_cast<uint8_t>(ctrl_meas_mask::osrs_t)));
  set_P_oversample(static_cast<osrs_p>(_old_ctrl_meas & static_cast<uint8_t>(ctrl_meas_mask::osrs_p)));

  return true;
}

Reading_base* BME280::readAll(void) {
  uint8_t data[8];
  if (!_read(Register::press_msb, data, 8))
    return nullptr;

  uint32_t rp = ((uint32_t)data[0] << 12) | ((uint32_t)data[1] << 4) | (data[2] >> 4);
  uint32_t rt = ((uint32_t)data[3] << 12) | ((uint32_t)data[4] << 4) | (data[5] >> 4);
  uint32_t rh = ((uint32_t)data[6] << 8) | data[7];

  return (Reading_base*)(new BME280Reading(this, rt, rp, rh));
}

Reading_base* BME280::readTemperature(Reading_base* reading) {
  uint8_t data[3];
  if (!_read(Register::temp_msb, data, 3))
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BME280Reading(this));
  reading->_set_raw_temp(((uint32_t)data[0] << 12) | ((uint32_t)data[1] << 4) | (data[2] >> 4));

  return reading;
}

Reading_base* BME280::readPressure(Reading_base* reading) {
  uint8_t data[3];
  if (!_read(Register::press_msb, data, 3))
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BME280Reading(this));
  reading->_set_raw_press(((uint32_t)data[0] << 12) | ((uint32_t)data[1] << 4) | (data[2] >> 4));

  return reading;
}

Reading_base* BME280::readHumidity(Reading_base* reading) {
  uint16_t rh = _read<uint16_t>(Register::hum_msb);
  if (_i2c_error)
    return nullptr;

  if (reading == nullptr)
    reading = (Reading_base*)(new BME280Reading(this));
  reading->_set_raw_hum(rh);

  return reading;
}


BME280Reading::BME280Reading(BME280* s, uint32_t rt, uint32_t rp, uint32_t rh) :
  BMP280Reading(s, rt, rp),
  _sensor(s)
{}

float BME280Reading::humidity(void) {
  if (!_have_t_fine)
    temperature();

  float var = _t_fine - 76800.0;
  var = (_rh - (_sensor->_dig_h4 + (_sensor->_dig_h5 * var))) * (_sensor->_dig_h2 * (1.0 + (_sensor->_dig_h6 * var * (1.0 + (_sensor->_dig_h3 * var)))));
  return var * (1.0 - (_sensor->_dig_h1 * var));
}
