#######################################
# Syntax Coloring Map For BMPEx80
#######################################
# Datatypes (KEYWORD1)
#######################################

TempSensor_base
PressSensor_base
HumSensor_base

BMP085
BMP180
BMP280
BMP380
BMP388

BME280
BME680


#######################################
# Methods and Functions (KEYWORD2)
#######################################

measureTemperature
measurePressure
measureHumidity

readRawTemperature
readRawPressure
readRawHumidity

rawTemperature
rawPressure
rawHumidity

temperature
pressure
humidity


#######################################
# Constants (LITERAL1)
#######################################


